#!/bin/sh

if [ ! -f /var/run/qubes-service/clocksync ]; then
    # https://github.com/QubesOS/qubes-issues/issues/7265
    /usr/bin/qvm-sync-clock
fi
