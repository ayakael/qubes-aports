# Use the update proxy over the QubesOS RPC for apk
# /etc/init.d/qubes-updates-proxy-forwarder creates the socket to the proxy
alias apk='https_proxy="http://127.0.0.1:8082/" http_proxy="http://127.0.0.1:8082/"  apk'
# allow aliases with sudo
alias sudo='sudo '
