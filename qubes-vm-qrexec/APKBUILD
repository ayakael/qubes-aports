# Contributor: Antoine Martin (ayakael) <dev@ayakael.net>
# Maintainer: Antoine Martin (ayakael) <dev@ayakael.net>
pkgname=qubes-vm-qrexec
subpackages="$pkgname-openrc $pkgname-doc"
pkgver=4.1.24
_gittag="v$pkgver"
pkgrel=2
pkgdesc="The Qubes qrexec files (qube side)"
arch="x86_64"
url="https://github.com/QubesOS/qubes-core-qrexec"
license='GPL'
depends="qubes-libvchan-xen"
options="!check" # No testsuite
makedepends="
	grep
	make
	lsb-release-minimal
	pandoc
	pkgconf
	py3-setuptools
	qubes-libvchan-xen-dev
	"
source="
	$pkgname-$_gittag.tar.gz::https://github.com/QubesOS/qubes-core-qrexec/archive/refs/tags/$_gittag.tar.gz
	qubes-qrexec-agent.openrc
	makefile-remove-cc-cflags.patch
	agent-qrexec-fork-server-undef-fortify-source.patch
	"
builddir="$srcdir/qubes-core-qrexec-${_gittag/v}"

prepare() {
	default_prepare
	# remove all -Werror
	msg "Eradicating -Werror..."
	find . \( -name '*.mk' -o -name 'Make*' \) -exec sed -i -e 's/-Werror//g' {} +
}

build() {
	make all-base
	make all-vm

	# change all shebangs to bash as expected
	# shellcheck disable=SC2013
	for i in $(grep '/bin/sh' -RlI .); do
		sed -i 's|/bin/sh|/bin/bash|' "$i"
	done
}

package() {
	make install-base DESTDIR="$pkgdir" SBINDIR=/sbin LIBDIR=/usr/lib SYSLIBDIR=/lib
	make install-vm DESTDIR="$pkgdir" SBINDIR=/sbin LIBDIR=/usr/lib SYSLIBDIR=/lib
	install -Dm 755 "$srcdir"/qubes-qrexec-agent.openrc "$pkgdir"/etc/init.d/qubes-qrexec-agent
}
sha512sums="
c263c2ebc878de41c9f8bd495855955096c5c69c2e48006170a3117793a64cfd1c65eee9c8b85dfca15b45f9e49db2cf8acf7bea51433708fffa49dcde8083a0  qubes-vm-qrexec-v4.1.24.tar.gz
e2dd5cace82e881c40d5d37c69f7327fbabde81c9d23283de23de9f1197b7b018ef07a8d90e95c61bd249426d9d8297e7cb372333245941ffa0682c90ea3461f  qubes-qrexec-agent.openrc
e48a06778a880915827fb2ef3e38379eb2bc6cf63f7fed79472be4732f7110b0c642c7a62a43236f53404ce69afddd40a5bc92a984403aae74caae1580c31200  makefile-remove-cc-cflags.patch
69b88c8d344f0d575eac398937040ba39a0d8fb8ea0a2b160c48d84775e1da4e226a76f3c5d3be7b045f577b634bb35cd5c5536248e18117c4121a38f9f3bf13  agent-qrexec-fork-server-undef-fortify-source.patch
"
