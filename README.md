# qubes-aports
Upstream: https://lab.ilot.io/ayakael/qubes-aports

## Description

This repository contains aports that allow Alpine Linux to be used as an Alpine
Linux template. The upstream repo uses GitLab's CI to build and deploy packages
targetting multiple Alpine Linux versions. QubesOS releases are tracked using
branches.

#### Template builder
The template builder is housed in its [own repo](https://lab.ilot.io/ayakael/qubes-builder-alpine).
RPMs are built in-pipeline using the build artifacts produced by this repo. These RPMs facilitate
installation of your very own Alpine Linux template on QubesOS.

#### Provided packages

Use `abuild-r` to build the following packages.
For more information on how to build an Alpine Package, read [this](https://wiki.alpinelinux.org/wiki/Creating_an_Alpine_package)

Core VM packages
  * qubes-vm-xen - Qubes's version of xen
  * qubes-libvchan-xen - libvchan library dependency
  * qubes-db-vm - qubes-db package
  * qubes-vm-utils - qubes-meminfo-writer service package
  * qubes-vm-core - Core init.d / qubes scripts
  * qubes-vm-gui-dev - Library dependencies for `qubes-vm-gui`
  * qubes-vm-gui - GUI agent
  * qubes-vm-qrexec - qrexec agent
  * qubes-gpg-split
  * qubes-usb-proxy
  * qubes-meta-packages - Meta package that pulls everything when added to world

Extra packages
  * qubes-pass - Aport for Rudd-O's inter-VM password manager for Qubes OS

Omitted packages
  * qubes-vmm-xen - The default Alpine xen package seems to provide the necessary modules

#### Known issues
Known issues are currently being tracked in [qubes-builder-alpine](https://lab.ilot.io/ayakael/qubes-builder-alpine) repo.

#### Issues, recommendations and proposals
**To report an issue or share a recommendation**

Go [here](https://gitlab.alpinelinux.org/ayakael/qubes-aports/-/issues)

**To make a merge request**
 * Fork the repo from Alpine's GitLab [here](https://gitlab.alpinelinux.org/ayakael/qubes-aports)
 * Clone your fork locally. (`git clone $repo`)
 * Make a branch with a descriptive name (`git checkout -b $descriptivename`)
 * Make the changes you want to see in the world, commit, and push to the GitLab's remote repo
 * Request a merge [here](https://gitlab.alpinelinux.org/ayakael/qubes-aports/-/merge_requests)
