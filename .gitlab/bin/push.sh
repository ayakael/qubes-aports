#!/bin/sh

# shellcheck disable=SC3043

. $CI_PROJECT_DIR/.gitlab/bin/functions.sh

# shellcheck disable=SC3040
set -eu -o pipefail

readonly APORTSDIR=$CI_PROJECT_DIR
readonly REPOS="backports user"
readonly BASEBRANCH=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME

export GIT_SSH_COMMAND="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"

gitlab_key_to_rsa $ABUILD_KEY rsa-private $HOME/.abuild/$ABUILD_KEY_NAME.rsa
gitlab_key_to_rsa $ABUILD_KEY_PUB rsa-public $HOME/.abuild/$ABUILD_KEY_NAME.rsa.pub
gitlab_key_to_rsa $SSH_KEY rsa-private $HOME/.ssh/id_rsa
chmod 700 "$HOME"/.ssh/id_rsa
chmod 700 "$HOME"/.abuild/$ABUILD_KEY_NAME.rsa

echo "PACKAGER_PRIVKEY=$HOME/.abuild/$ABUILD_KEY_NAME.rsa" > $HOME/.abuild/abuild.conf
echo "REPODEST=$HOME/repo-apk/qubes" >> $HOME/.abuild/abuild.conf
sudo cp $HOME/.abuild/$ABUILD_KEY_NAME.rsa.pub /etc/apk/keys/.

get_qubes_release() {
	case $BASEBRANCH in
		r*) echo $BASEBRANCH;;
		master) echo r4.2;;
		*) die "Branch \"$BASEBRANCH\" not supported!"
	esac
}

QUBES_REL=$(get_qubes_release)

for release in $(find packages -type d -maxdepth 1 -mindepth 1 -printf '%f\n'); do

	if [ -d $HOME/repo-apk ]; then
		git -C $HOME/repo-apk fetch
		git -C $HOME/repo-apk checkout $release
		git -C $HOME/repo-apk pull --rebase
	else
		git clone git@lab.ilot.io:ayakael/repo-apk -b $release $HOME/repo-apk
	fi

	for i in $(find packages/$release -type f -name "*.apk"); do
		install -vDm644 $i ${i/packages\/$release\/qubes-aports/$HOME\/repo-apk\/qubes\/$QUBES_REL}
	done

	fetch_flags="-qn"
	git fetch $fetch_flags "$CI_MERGE_REQUEST_PROJECT_URL" \
		"+refs/heads/$BASEBRANCH:refs/heads/$BASEBRANCH"

	rm $HOME/repo-apk/qubes/$QUBES_REL/*/APKINDEX.tar.gz || true
	mkdir -p qubes/$QUBES_REL/DUMMY
	echo "pkgname=DUMMY" > qubes/$QUBES_REL/DUMMY/APKBUILD
	cd qubes/$QUBES_REL/DUMMY
	abuild index
	cd "$CI_PROJECT_DIR"
	rm -R qubes/$QUBES_REL/DUMMY

	git -C $HOME/repo-apk add .
	git -C $HOME/repo-apk commit -m "Update from $CI_MERGE_REQUEST_IID - $CI_MERGE_REQUEST_TITLE"
	git -C $HOME/repo-apk push
done
